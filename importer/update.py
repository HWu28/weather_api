import json

import psycopg2
import requests
from requests.exceptions import HTTPError

from config import config


def database_connection(commands):
    conn = None
    try:
        # read the connection parameters
        params = config('database.ini')
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            if isinstance(command, str):
                cur.execute(command)
            else:
                cur.execute(command[0], command[1])
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def database_select(command):
    conn = None
    try:
        # read the connection parameters
        params = config('database.ini', 'weather_db')
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table
        if isinstance(command, str):
            cur.execute(command)
        else:
            cur.execute(command[0], command[1])
        answer = cur.fetchall()
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return answer


def get_cities_list(country_code='ES'):
    # look for list of cities in country
    query = ("""
            select name,
                   country_name,
                   country_code,
                   lat,
                   long,
                   id
            from city
            where (country_code = %s)
            """, (country_code,))
    return database_select(query)


def get_weather_coordinates(lat, long):
    # load tutiempo api credentials
    json_file = open('credentials.json')
    api_key = json.load(json_file)['API_KEY_tu_tiempo']
    # tutiempo api call
    url = 'https://api.tutiempo.net/json/?lan=es&apid=' + api_key + \
          '&ll=' + str(lat) + ',' + str(long)
    try:
        response = requests.get(url)
        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'Other error occurred: {err}')
    else:
        return response.json()


def update_weather_tutiempo(city_id, json_file):
    try:
        query_answer = json_file['hour_hour']['hour1']
        # insert into table weather the last data from next hour of a city
        commands = [("""
                    INSERT INTO weather (city_id,text,temperature,humidity,pressure,wind,
                        wind_direction)
                    VALUES(%s,%s,%s,%s,%s,%s,%s)
                    ON CONFLICT (city_id)
                    DO UPDATE SET text = EXCLUDED.text,
                        temperature = EXCLUDED.temperature,
                        humidity = EXCLUDED.humidity,
                        pressure = EXCLUDED.pressure,
                        wind = EXCLUDED.wind,
                        wind_direction = EXCLUDED.wind_direction """,
                     (int(city_id), query_answer['text'], int(query_answer['temperature']),
                      int(query_answer['humidity']), int(query_answer['pressure']), int(query_answer['wind']),
                      query_answer['wind_direction']))]
        database_connection(commands)
    except KeyError as not_found_error:
        print('Not found city ' + str(city_id))
    except Exception as error:
        print('Error ' + str(error))


if __name__ == '__main__':
    for city in get_cities_list():
        api_answer = get_weather_coordinates(city[3], city[4])
        choose_city = city[5]
        update_weather_tutiempo(choose_city, api_answer)
    print("Updated cities weather data")
