import requests


def test_get_root_available():
    response = requests.get("http://127.0.0.1:8001/")
    assert response.status_code == 200


def test_get_root_response_format():
    response = requests.get("http://127.0.0.1:8001/")
    assert response.headers["Content-Type"] == "application/json"


def test_get_root_response_info():
    response = requests.get("http://127.0.0.1:8001/")
    response_body = response.json()
    assert response_body["api"] == "Weather API"


def test_get_cities_list_available():
    response = requests.get("http://127.0.0.1:8001/cities/")
    assert response.status_code == 200


def test_get_cities_list_response_format():
    response = requests.get("http://127.0.0.1:8001/cities/")
    assert response.headers["Content-Type"] == "application/json"


def test_get_cities_list_response_city_id():
    response = requests.get("http://127.0.0.1:8001/cities/")
    response_body = response.json()
    for city in response_body:
        if city[0] == "Barcelona":
            assert city[5] == 164
        elif city[0] == "Madrid":
            assert city[5] == 239


def test_get_weather_available():
    response = requests.get("http://127.0.0.1:8001/weather/Barcelona")
    assert response.status_code == 200


def test_get_weather_response_format():
    response = requests.get("http://127.0.0.1:8001/weather/Barcelona")
    assert response.headers["Content-Type"] == "application/json"


def test_get_weather_response_info():
    response = requests.get("http://127.0.0.1:8001/weather/Barcelona")
    response_body = response.json()
    assert response_body["city"] == "Barcelona"


def test_get_weather_available_wrong_name():
    response = requests.get("http://127.0.0.1:8001/weather/Brcelona")
    assert response.status_code == 200


def test_get_weather_response_format():
    response = requests.get("http://127.0.0.1:8001/weather/Brcelona")
    assert response.headers["Content-Type"] == "application/json"


def test_get_weather_response_info():
    response = requests.get("http://127.0.0.1:8001/weather/Brcelona")
    response_body = response.json()
    assert response_body["Error"] == "City Not Found"

