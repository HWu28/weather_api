FROM python
COPY ./ /opt/project
RUN pip3 install -r /opt/project/requirements.txt
