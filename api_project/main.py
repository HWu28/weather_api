from fastapi import FastAPI
from config import config
import psycopg2

app = FastAPI()


def database_select(command):
    conn = None
    try:
        # read the connection parameters
        params = config('database.ini', 'weather_db')
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # execute the query
        if isinstance(command, str):
            cur.execute(command)
        else:
            cur.execute(command[0], command[1])
        answer = cur.fetchall()
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return answer


def read_city_id(city: str, country_code='ES'):
    # look for city in database
    query = ("""
            select name,
                   id
            from city
            where (country_code = %s and name = %s)
            """, (country_code, city))
    answer = database_select(query)

    if not answer:
        return {"Error": "City not found"}
    else:
        return answer


@app.get("/")
def read_root():
    return {"api": "Weather API", "info": {"source": "https://www.tutiempo.net"}}


@app.get("/cities/")
def read_cities(country_code='ES'):
    # look for cities from the country
    query = ("""
            select name,
                   country_name,
                   country_code,
                   lat,
                   long,
                   id
            from city
            where (country_code = %s)
            """, (country_code,))
    return database_select(query)


@app.get("/weather/{city}")
def read_weather(city: str):
    try:
        city_id = read_city_id(city)[0]
        # look for last weather info from a city
        query = ("""
                    select *
                    from weather
                    where (city_id = %s)
                    """, (city_id[1],))
        answer = database_select(query)[0]
        return {"city": city, "weather": answer[2], "temperature": answer[3], "humidity": answer[4],
                "pressure": answer[5], "wind": answer[6], "wind_direction": answer[7]}
    except KeyError as Error:
        return {"Error": 'City Not Found'}
    except Exception as Error:
        return {"Error": Error}

