import json
import requests
from requests.exceptions import HTTPError
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from config import config
import csv


def create_database():
    # Connect to PostgreSQL DBMS
    params = config('database.ini', 'postgresql_0')
    con = psycopg2.connect(**params)
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = con.cursor()
    name_database = "weather_db"
    # Create table statement
    sql_create_database = "create database "+name_database+";"
    # Create a table in PostgreSQL database
    cursor.execute(sql_create_database)


def check_database():
    params = config('database.ini', 'postgresql_0')
    # connect to database
    con = psycopg2.connect(**params)
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = con.cursor()
    # check if exist database with name weather_db
    cursor.execute(
        """
            SELECT 1 FROM pg_catalog.pg_database
            WHERE datname = 'weather_db'
        """)
    exists = cursor.fetchone()
    if not exists:
        return False
    else:
        return True


def database_connection(commands):
    conn = None
    try:
        # read the connection parameters
        params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # execute queries
        for command in commands:
            if isinstance(command, str):
                cur.execute(command)
            else:
                cur.execute(command[0], command[1])
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_weather_now_city(lat, long):
    # load credentials for tutiempo APi
    json_file = open('../credentials.json')
    api_key = json.load(json_file)['API_KEY_tu_tiempo']
    url = 'https://api.tutiempo.net/json/?lan=es&apid=' + api_key + \
          '&ll=' + str(lat) + ',' + str(long)
    try:
        # Connect to API
        response = requests.get(url)
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'Other error occurred: {err}')
    else:
        # Return the answer
        return response.json()


def create_tables():
    # sql queries to create the tables at the database
    commands = (
        """
        CREATE TABLE IF NOT EXISTS city (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            country_name VARCHAR(255) NOT NULL,
            country_code VARCHAR(255) NOT NULL,
            lat REAL NOT NULL,
            long REAL NOT NULL,
        UNIQUE (name, country_name,country_code)
        )
        """,
        """ CREATE TABLE IF NOT EXISTS weather (
                id SERIAL PRIMARY KEY,
                city_id INTEGER UNIQUE,
                text VARCHAR(255) NOT NULL,
                temperature INTEGER NOT NULL,
                humidity INTEGER NOT NULL,
                pressure INTEGER NOT NULL,
                wind INTEGER NOT NULL,
                wind_direction VARCHAR(255) NOT NULL
                )
        """)
    database_connection(commands)


def import_cities():
    # load cities from csv file
    cities = csv.reader(open('./importer/cities.csv'), delimiter=',')
    commands = []
    for city in cities:
        # sql queries to fill cities at the city table
        commands.append(("""
                INSERT INTO city (name,country_name, country_code,lat,long)
                VALUES(%s,%s,%s,%s,%s)
                ON CONFLICT (name,country_name,country_code)
                DO NOTHING""",
                (city[1], city[4], city[5], float(city[2]), float(city[3]))))

    database_connection(commands)


if __name__ == '__main__':
    if not check_database():
        create_database()
        create_tables()
        import_cities()
        print("Database created")
    else:
        print("Database already exist, it didn't changed")
