# Weather Api
## Project Instructions
[Backend Test](https://gitlab.com/HWu28/weather_api/-/blob/master/Backend%20Test.docx)
## Instructions

To run the Weather API:
1. Create a credentials.json file at the project root folder, [credentials_sample.json](https://gitlab.com/HWu28/weather_api/-/blob/master/credentials_sample.json)
2. Launch the Docker containers with the bash command 

```bash
docker-compose up
```

To run the unit test:
1. Launch all the Docker containers
2. Move to folder test
```bash
cd test
```
3. Install the needed libraries
```bash
pip3 install -r requirements.txt
```
4. Launch the tests
```bash
pytest test.py
```
### API calls
0. Weather API must be running
local project $url: http://localhost:8001/
1. API calls:  
   1.1.1 Call - http://localhost:8001/   
   1.1.2 Response - return basic information of service   
```json
{"api": "Weather API",
"info": {"source":"https://www.tutiempo.net"}
}
```
1.2.1 Call - http://localhost:8001/cities/  
1.2.2 Response - return list of cities from Spain at the database  
```json
{	
0:{0:"Barcelona",
1:"Spain",
2:"ES",
3:41.3825,
4:2.1769,
5:164},
1:{0:"Madrid",
1:"Spain",
2:"ES",
3:40.4189,
4:-3.6919,
5:239},
	...
}
```

1.3.1 Call - http://localhost:8001/weather/{city}  
1.3.2 Example - http://localhost:8001/weather/Barcelona  
1.3.3 Response - return weather information of city  
```json
{"city":"Barcelona",
"weather":"Despejado",
"temperature":11,
"humidity":84,
"pressure":1032,
"wind":5,
"wind_direction":"Noroeste"}
```


### Prerequisites & Installing

[Docker compose](https://docs.docker.com/compose/install/)  
[Python 3](https://www.python.org/)  
[Pip for Python3](https://linuxize.com/post/how-to-install-pip-on-ubuntu-18.04/)  
